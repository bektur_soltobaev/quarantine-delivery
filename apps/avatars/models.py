from django.db import models
from django.contrib.gis.db import models
from django.conf import settings
from django.contrib.postgres.fields import HStoreField


CHOICES = {
    (1, 'Нуждаюсь в доставке'),
    (2, 'Готов доставить')
}


class Avatar(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='avatar')
    status = models.IntegerField(choices=CHOICES, default=1)
    location = models.PointField(srid=4326)

    def __str__(self):
        return self.user.username + '\'s avatar'


class Link(models.Model):
    metadata = HStoreField(blank=True, null=True, default=dict)
    geo = models.LineStringField()
    # objects = models.GeoManager()

from django.apps import AppConfig


class AvatarsConfig(AppConfig):
    name = 'apps.avatars'

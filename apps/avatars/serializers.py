from rest_framework_gis.serializers import GeoFeatureModelSerializer
from .models import Avatar


class AvatarSerializer(GeoFeatureModelSerializer):

    class Meta:
        model = Avatar
        geo_field = 'location'
        auto_bbox = True
        fields = ('location', )

from django.urls import path
from .views import AvatarView

urlpatterns = [
    path('avatars/', AvatarView.as_view(), name='avatars_list_view')
]

from rest_framework import views, status
from rest_framework.response import Response
from django.contrib.gis.db.models.functions import Distance

from .serializers import AvatarSerializer
from .models import Avatar


class AvatarView(views.APIView):

    def get(self, request):
        user = request.user
        location = user.avatar.location
        queryset = Avatar.objects.annotate(distance=Distance('location', location)).order_by('distance')[0:3]
        return Response({'Closest avatars': [x.loctaion for x in queryset]}, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = AvatarSerializer(data=request.data)
        if serializer.is_valid():
            user = request.user
            location = serializer.validated_data['location']
            Avatar.objects.create(user=user, location=location)
            return Response('Your location is set!', status=status.HTTP_201_CREATED)
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

from rest_framework import serializers
from apps.userapi.models import User, UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('phone_number', 'location', 'date_joined', 'status')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    el_profile = UserProfileSerializer(required=True)

    def create(self, validated_data):
        profile_data = validated_data.pop('el_profile')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        UserProfile.objects.create(user=user, **profile_data)
        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('el_profile')
        el_profile = instance.profile

        instance.email = validated_data.get('email', instance.email)
        instance.save()

        el_profile.phone_number = profile_data.get(
            'phone_number', el_profile.phone_number)

        el_profile.location = profile_data.get('location', el_profile.location)

        el_profile.address = profile_data.get(
            'address', el_profile.address)
        el_profile.date_joined = profile_data.get(
            'date_joined', el_profile.date_joined)
        el_profile.save()

        return instance

    class Meta:
        model = User
        fields = ('url', 'email', 'first_name', 'last_name',
                  'password', 'el_profile')  # phone_number
        extra_kwargs = {'password': {'write_only': True}}

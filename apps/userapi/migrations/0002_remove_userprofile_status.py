# Generated by Django 3.0.5 on 2020-04-14 05:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userapi', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='status',
        ),
    ]

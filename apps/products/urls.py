from django.urls import path

from .views import CategoryViewSet, ProductViewSet, BasketView, BasketOutView

urlpatterns = [
    path('categories/', CategoryViewSet.as_view({
        'get': 'list', 'post': 'create'}),
        name='category_list'),
    path('categories/<int:id>/', CategoryViewSet.as_view({
        'get': 'retrieve', 'delete': 'destroy',
        'put': 'update', 'patch': 'partial_update'}),
        name='category_detail'),

    path('products/', ProductViewSet.as_view({'get': 'list', 'post': 'create'}),
         name='product_list'),
    path('products/<int:id>/', ProductViewSet.as_view({
        'get': 'retrieve', 'delete': 'destroy', 'put': 'update',
        'patch': 'partial_update'}),
        name='product_detail'),

    path('basket/', BasketView.as_view(), name='basket_url'),
    path('basket-out/', BasketOutView.as_view(), name='basket_out_url')
]

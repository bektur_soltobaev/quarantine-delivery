from rest_framework import serializers
from .models import Category, Product, Basket


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', )


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'


class ProductBasketSerializer(serializers.Serializer):
    product_id = serializers.IntegerField()
    amount_to_add = serializers.IntegerField()


class BasketSerializer(serializers.Serializer):
    products = ProductBasketSerializer(required=False, many=True)

    class Meta:
        model = Basket
        fields = ['products']

from django.contrib import admin
from .models import Category, Product, Basket

#
# class CategoryAdmin(admin.ModelAdmin):
#     list_display = ['name', 'slug']
#     prepopulated_fields = {'slug': ('name',)}
# admin.site.register(Category, CategoryAdmin)
#
#
#
# class ProductAdmin(admin.ModelAdmin):
#     list_display = ['name', 'slug', 'description', 'available']
#     list_filter = ['available']
#     list_editable = ['available']
#     prepopulated_fields = {'slug': ('name',)}
# admin.site.register(Product, ProductAdmin)
#
# class BasketAdmin(admin.ModelAdmin):
#     list_display = ['user']
#
# admin.site.register(Basket, BasketAdmin)

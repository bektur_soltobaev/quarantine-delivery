from django.db import models
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save

User = get_user_model()


class Category(models.Model):
    name = models.CharField(max_length=100, db_index=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', verbose_name='Категория', on_delete=models.CASCADE)
    name = models.CharField(max_length=100, db_index=True, verbose_name='Название продукта')
    thumbnail = models.ImageField(blank=True, null=True)
    price = models.IntegerField(default=0)
    in_basket = models.ManyToManyField('Basket', related_name='products')

    def __str__(self):
        return self.name


class Basket(models.Model):
    user = models.OneToOneField(User, related_name='basket', on_delete=models.CASCADE, null=True, blank=True)

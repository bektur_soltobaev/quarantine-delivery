from django.apps import AppConfig


class TherealproductsConfig(AppConfig):
    name = 'apps.therealproducts'

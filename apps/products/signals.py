from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Basket

User = get_user_model()


@receiver(post_save, sender=User)
def create_user_basket(sender, instance, created, **kwargs):
    if created:
        Basket.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_basket(sender, instance, **kwargs):
    instance.basket.save()

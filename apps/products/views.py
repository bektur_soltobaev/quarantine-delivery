from rest_framework import viewsets
from .models import Category, Product, Basket
from .serializers import CategorySerializer, ProductSerializer, BasketSerializer
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    lookup_field = 'id'
    queryset = Category.objects.all()


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    lookup_field = 'id'
    queryset = Product.objects.all()


class BasketView(APIView):

    def post(self, request):
        serializer = BasketSerializer(data=request.data)
        if serializer.is_valid():
            user = request.user
            session = request.session
            if user.id is not None:
                basket = user.basket
            else:
                session['basket'] = Basket.objects.create().id
                basket = Basket.objects.get(id=session['basket'])
            products = serializer.validated_data['products']

            for product in products:
                obj = Product.objects.get(id=product['products_id'])
                basket.products.add(obj)

            return Response({'products in basket': [x.id for x in basket.products.all()]})
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class BasketOutView(APIView):

    def post(self, request):
        serializer = BasketSerializer
        if serializer.is_valid():
            user = request.user
            session = request.session
            if session['basket']:
                basket = Basket.objects.get(id=session['basket'])
            else:
                basket = user.Basket
            products = serializer.validated_data['products']

            for product in products:
                obj = basket.get(id=product['product_id'])
                basket.products.remove(obj)

            return Response({'products in basket': [x.id for x in basket.products.all()]})
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

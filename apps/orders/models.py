import itertools
from django.db import models
from django.utils.text import slugify

from apps.products.models import Basket


class Order(models.Model):
    slug = models.CharField(max_length=56, blank=True)
    basket = models.ForeignKey(Basket, on_delete=models.SET_NULL, blank=True, null=True)

    def _generate_slug(self):
        max_length = self._meta.get_field('slug').max_length
        if self.basket.user is not None:
            value = self.basket.user.username
        else:
            value = 'anonimoususer'
        slug_candidate = slug_original = slugify(value, allow_unicode=True)
        for i in itertools.count(1):
            if not Order.objects.filter(slug=slug_candidate).exists():
                break
            slug_candidate = '{}-{}'.format(slug_original, i)
        self.slug = slug_candidate

    def save(self, *args, **kwargs):
        if not self.pk:
            self._generate_slug()

        super().save(*args, **kwargs)
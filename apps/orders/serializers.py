from rest_framework import serializers

from .models import Order


class OrderSerializer(serializers.ModelSerializer):

    slug = serializers.CharField(required=False)

    class Meta:
        model = Order
        fields = ('slug', )

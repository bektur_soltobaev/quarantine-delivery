from rest_framework.views import APIView, status
from django.contrib.gis.db.models.functions import Distance
from rest_framework.response import Response

from .models import Order
from .serializers import OrderSerializer
from apps.avatars.models import Avatar


class OrderView(APIView):

    def post(self, request):
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            user = request.user
            basket = user.basket
            Order.objects.create(bakset=basket)
            location = user.avatar.location
            user.avatar.status = 1
            avatars = Avatar.objects.annotate(distance=Distance('location', location)).order_by('distance')[0:3]
            return Response({'Closest avatars': [x.loctaion for x in avatars]}, status=status.HTTP_200_OK)
        return Response({'details': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

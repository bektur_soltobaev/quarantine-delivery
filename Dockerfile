FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
RUN apt-get update &&\
    apt-get install -y binutils libproj-dev gdal-bin\
    && apt-get --assume-yes install gcc python3-dev musl-dev \
    && apt-get --assume-yes install libgeos-dev binutils libproj-dev gdal-bin libgdal-dev python3-gdal \
    && apt-get install -y sqlite3 libsqlite3-dev
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
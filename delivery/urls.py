from django.contrib import admin
from django.urls import path, include

from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token

from rest_framework.documentation import include_docs_urls  #new

from django.conf import settings
from django.conf.urls.static import static

API_TITLE = 'UTLab API'  # new
API_DESCRIPTION = 'Web API для создания и редактирования...'  # new


urlpatterns = [
    path('admin/', admin.site.urls),
    #   это просто во время разработки нужно
    path('api-auth/', include('rest_framework.urls')),
    #   jwt
    path('api-token-auth/', obtain_jwt_token),
    path('api-token-refresh/', refresh_jwt_token),
    path('api-token-verify/', verify_jwt_token),
    #   authentication
    path('', include('django.contrib.auth.urls')),
    #   apps
    path('userapi/', include('apps.userapi.urls')),
    path('api/v1/', include('apps.products.urls')),
    path('api/v1/', include('apps.avatars.urls')),
    path('api/v1/', include('apps.orders.urls')),
    #   Документации
    path('docs/', include_docs_urls(title=API_TITLE,
                                    description=API_DESCRIPTION)),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
